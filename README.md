# Virt RabbitMQ example

## Usage

Install dependencies in a suitable way for you environment, e.g. for a venv do

```shell
pip install -e .
```

Import the configuration into you current shell session:

```shell
. .env
```

Add the listener as a webhook to a GitLab project. The URL and secret can be
found in the `RABBITMQ_VIRT_WEBHOOK` and `RABBITMQ_VIRT_WEBHOOK_SECRET`
variables.

Start the listener:

```shell
python3 -m virt_rabbitmq_example
```

Do something in the project that will trigger a webhook.

You can nose around the admin interface via the account info in
`RABBITMQ_VIRT_MANAGER_USER` and `RABBITMQ_VIRT_MANAGER_PASSWORD` at
`https://$RABBITMQ_VIRTUAL_HOST:$RABBITMQ_MANAGEMENT_PORT`.
